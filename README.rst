Python 2.7 SDK
==============

- version: 1.0
- compatible with: API version 1.3
- Python version: 2.7.13

Dependencies:
-------------

- pycrypto
- requests

Installation:
-------------

1. Clone this repository.

2. Import into your app:

.. code-block:: python

  from connection import Connection

Usage:
------

==================================
Initializing a `Connection` object
==================================

.. code-block:: python

  wallet = Connection(host, consumerKey, consumerSecret)

==================================
Consuming public resources
==================================

.. code-block:: python

  wallet = Connection(host, consumerKey, consumerSecret)

  # List all card types
  response = wallet.consume('users/wallets/cards/types', 'GET')
  # Raise requests error
  response.raise_for_status()
  card_types = response.json()

  # Register a user
  data = {
      'email': 'me@email.com',
      'password': 'password12',
      'first_name': 'John',
      'last_name': 'Doe',
      'mobile_country_code': '65',
      'mobile': '98765432',
  }
  response = wallet.consume('users', 'POST', data)
  # Raise requests error
  response.raise_for_status()
  user = response.json()

==================================
"Logging in" / User authentication
==================================

.. code-block:: python

  wallet = Connection(host, consumerKey, consumerSecret)
  wallet.authenticate(user, password)

==================================
Consume Authenticated resources
==================================

.. code-block:: python

  wallet = Connection(host, consumerKey, consumerSecret)
  wallet.authenticate(user, password)
  
  # Get user details
  response = wallet.consume('users', 'GET', nil)
  # Raise requests error
  response.raise_for_status()
  user = response.json()
  
  # Update user's details
  data = {
      'title': 'Mr',
      'gender': 'male',
      'id_number': 's1234567890',
      'id_type': 'nric',
      'country_of_issue': 'Singapore',
  }
  response = wallet.consume('users', 'PUT', data)
  # Raise requests error
  response.raise_for_status()
  user = response.json()

More Examples:
--------------

- `demo.py <https://bitbucket.org/matchmove/api-vcard-sdk-python/src/507436c42c266366629982c081026e304a418b33/demo.py?at=master&fileviewer=file-view-default>`_
- run "python demo.py"

Issues
------

`Any inquiries, issues or feedbacks? <https://bitbucket.org/matchmove/api-vcard-sdk-python/issues>`_
