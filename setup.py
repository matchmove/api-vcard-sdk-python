# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read()

setup(
    name = 'vcard-python-sdk',
    version = '1.0.0',
    keywords = 'matchmove vcard-sdk',
    description = 'A Python SDK for MatchMove API',
    long_description = readme,
    author = 'Lord Rainiel Mangila',
    author_email = 'lord@matchmove.com',
    url = 'https://bitbucket.org/matchmove/api-vcard-sdk-python',
    packages = find_packages(
        exclude = (
            'tests',
            'docs'
        )
    ),
    install_requires = [
        'nose',
        'pycrypto',
        'requests'
    ]
)
