# -*- coding: utf-8 -*-

from .securities import Securities
from .utilities import Utilities

import hashlib, time

class OAuth(object):
    
    global algo, encoding
    
    algo = 'md5'
    encoding = 'utf-8'
    
    def __init__(self):
        self.utilities = Utilities()
    
    def get_nonce(self):
        return hashlib.md5(
            str(time.time()) +
            str(self.utilities.mt_rand()) +
            str(self.utilities.microtime())
        ).hexdigest()
    
    def urlencode(self, value):
        if not self.utilities.is_array(value):
            return value.decode(
                self.utilities.mb_detect_encoding(value)
            ).encode(encoding).replace('%E7', '~').replace('+', '%20')
        
        query = []
        for index in range(len(value)):
            query.append(value[index])
        
        return '&'.join([str(i) for i in query])
    
    def encrypt(self, secret, signature, payload):
        if self.utilities.is_array(secret):
            secret = ''.join([str(i) for i in secret])
        
        key = hashlib.md5(str(signature) + str(secret)).hexdigest()
        
        self.securities = Securities(key)
        return self.securities.encrypt(payload)
