# -*- coding: utf-8 -*-

import math, operator, random, sys, time

class Utilities(object):
    
    global encoding_list
    
    encoding_list = ['utf-8', 'latin1', 'iso8859-15']
    
    def is_array(self, value):
        return isinstance(value, (list, tuple))
    
    def ksort(self, d):
        return [(k, d[k]) for k in sorted(d.keys())]
    
    def mt_rand(self, low = 0, high = sys.maxint):
        '''
        Generate a better random value
        '''
        return random.randint(low, high)
    
    def microtime(self, get_as_float = False):
        if get_as_float:
            return time.time()
        
        # Return the integer part of math.modf
        return '%d' % math.modf(time.time())[1]
    
    def mb_detect_encoding(self, text, list = None):
        if list is None:
            list = encoding_list
        
        '''
        Return first matched encoding in encoding_list, otherwise return None.
        See [url]http://docs.python.org/2/howto/unicode.html#the-unicode-type[/url] for more info.
        See [url]http://docs.python.org/2/library/codecs.html#standard-encodings[/url] for encodings.
        '''
        for best_enc in list:
            try:
                unicode(text, best_enc)
            except:
                best_enc = None
            else:
                break
        
        return best_enc
