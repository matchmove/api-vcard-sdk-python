# -*- coding: utf-8 -*-

from Crypto.Hash import SHA, HMAC
from helpers import Utilities, OAuth

import base64, hashlib, hmac, httplib, requests, time, urllib

class Connection(object):
    
    global _headers, _oauth_version, _signature_method
    global _token_access_uri, _token_request_uri
    
    _headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    
    _oauth_version = '1.0'
    _signature_method = 'HMAC-SHA1'
    _token_access_uri  = 'oauth/access/token'
    _token_request_uri = 'oauth/request/token'
    
    def __init__(self, host, key, secret, ssl_certificate = None):
        self.access = None
        self.uri = None
        self.host = host 
        self.key = key
        self.ssl_certificate = ssl_certificate
        self.secret = secret
        self.utilities = Utilities()
        self.oauth = OAuth()
    
    def authenticate(self, username, password):
        try:
            request_token = self.get_request_token(username, password)
        except Exception, e:
            raise e
        
        self.access = self.get_access_token(request_token)
        
        return self.access
    
    def consume(self, api, method, query = {}):
        self.uri = str(self.host + '/' + api)
        
        if self.access is None:
            query = self._prepare_consume_public(method, api, query)
        else:
            query = self._prepare_consume_oauth(method, api, query)
        
        try:
            return getattr(self, method.lower())(self.uri, query)
        except:
            return AttributeError
    
    def get(self, url, data):
        if self.ssl_certificate is None:
            return requests.get(url, params = data, headers = _headers)
        
        return requests.get(
            url,
            params = data,
            headers = _headers,
            verfiy = self.ssl_certificate
        )
    
    def post(self, url, data):
        if self.ssl_certificate is None:
            return requests.post(url, data = data, headers = _headers)
        
        return requests.post(
            url,
            data = data,
            headers = _headers,
            verfiy = self.ssl_certificate
        )
    
    def put(self, url, data):
        if self.ssl_certificate is None:
            return requests.put(url, params = data, headers = _headers)
        
        return requests.put(
            url,
            params = data,
            headers = _headers,
            verfiy = self.ssl_certificate
        )
    
    def delete(self, url, data):
        if self.ssl_certificate is None:
            return requests.delete(url, params = data, headers = _headers)
        
        return requests.delete(
            url,
            params = data,
            headers = _headers,
            verfiy = self.ssl_certificate
        )
    
    def get_default_query(self, query = {}):
        default = {
            'oauth_consumer_key': self.key,
            'oauth_nonce': self.oauth.get_nonce(),
            'oauth_signature_method': _signature_method,
            'oauth_timestamp': self.get_timestamp(),
            'oauth_version': _oauth_version
        }
        return dict(default, **query)
    
    def get_timestamp(self):
        return str(int(time.time()))
    
    def get_signature(self, secret, method, url, query = {}):
        if isinstance(query, dict):
            query = self.oauth.urlencode(
                urllib.urlencode(self.utilities.ksort(query))
            )
        
        if method != None:
            method = self.oauth.urlencode(method)
        
        if self.uri != None:
            self.uri = self.oauth.urlencode(self.uri)
        
        array = [method, self.uri, query]
        contents = '&'.join([urllib.quote(str(i), safe = '') for i in array])
        
        if self.utilities.is_array(secret):
            secret = '&'.join([str(i) for i in secret])
        
        h = hmac.new(secret, contents, hashlib.sha1)
        # Raw sha1 output, change this to .hexdigest()
        signature = h.digest()
        
        return base64.b64encode(signature)
    
    def get_request_token(self, username, password):
        query = self.get_default_query({
            'oauth_user_name': username,
            'oauth_user_password': password
        })
        
        self.uri = str(self.host + '/' + _token_request_uri)
        query['oauth_signature'] = self.get_signature(
            self.secret,
            'POST',
            _token_request_uri,
            query
        )
        
        query['oauth_user_name'] = self.oauth.encrypt(
            self.secret,
            query['oauth_signature'],
            username
        )
        
        query['oauth_user_password'] = self.oauth.encrypt(
            self.secret,
            query['oauth_signature'],
            password
        )
        
        url = str(self.host + '/' + _token_request_uri)
        response = self.post(url, query)
        response.raise_for_status()
        json_data = response.json()
        if 'oauth_token' not in json_data:
            raise ValueError('Request token\'s response is invalid or empty')
        
        return json_data
    
    def get_access_token(self, request):
        if 'oauth_token' not in request:
            raise ValueError('Request token\'s response is invalid or empty')
        
        query = self.get_default_query({
            'oauth_token': request['oauth_token']
        })
        
        self.uri = str(self.host + '/' + _token_access_uri)
        query['oauth_signature'] = self.get_signature(
            [self.secret, request['oauth_token_secret']],
            'POST',
            _token_access_uri,
            query
        )
        
        url = str(self.host + '/' + _token_access_uri)
        response = self.post(url, query)
        response.raise_for_status()
        json_data = response.json()
        if 'oauth_token' not in json_data:
            raise ValueError('Request token\'s response is invalid or empty')
        
        return json_data
    
    def _prepare_consume_oauth(self, method, api, query):
        if 'oauth_token' not in self.access:
            raise ValueError('Request token\'s response is invalid or empty')
        
        query['oauth_token'] = self.access['oauth_token']
        query = self.get_default_query(query)
        
        query['oauth_signature'] = self.get_signature(
            [self.secret, self.access['oauth_token_secret']],
            method,
            api,
            query
        )
        
        return self.utilities.ksort(query)
    
    def _prepare_consume_public(self, method, api, query):
        urlencode = urllib.urlencode(query)
        unquote_plus = urllib.unquote_plus(urlencode)
        query = self.get_default_query({
            '_d': unquote_plus
        })
        
        query['oauth_signature'] = self.get_signature(
            self.secret,
            method,
            api,
            query
        )
        
        query['_d'] = self.oauth.encrypt(
            self.secret,
            query['oauth_signature'],
            query['_d']
        )
        
        return self.utilities.ksort(query)
