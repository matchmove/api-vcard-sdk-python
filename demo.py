# -*- coding: utf-8 -*-

from connection import Connection

import os

class Demo:
    
    def __init__(self):
        number = '470256213022'
        username = 'myuser' + number + '@matchmove.com'
        
        self.credentials = {
            'host': os.environ['VCARD_HOST'] + '/sgmc/v1',
            'key': os.environ['VCARD_KEY'],
            'secret': os.environ['VCARD_SECRET'],
            'username': username,
            'password': 'mypassword1111',
            'number': number
        }
        
        # Initialize a public connection
        self.pub_conn = Connection(
            self.credentials['host'],
            self.credentials['key'],
            self.credentials['secret']
        )
        
        # Initialize another connection for authenticated resources
        self.conn = Connection(
            self.credentials['host'],
            self.credentials['key'],
            self.credentials['secret']
        )
    
    def authenticate(self):
        # Authenticate the initialized connection
        return self.conn.authenticate(
            self.credentials['username'],
            self.credentials['password']
        )
    
    def get_card_types(self):
        return self.pub_conn.consume('users/wallets/cards/types', 'GET')
    
    def post_users(self):
        data = {
            'email': self.credentials['username'],
            'password': self.credentials['password'],
            'first_name': 'Fname',
            'last_name': 'Lname',
            'preferred_name': 'Fname Lname',
            'mobile_country_code': 65,
            'mobile': self.credentials['number']
        }
        return self.pub_conn.consume('users', 'POST', data)
    
    def put_users(self, data):
        return self.conn.consume('users', 'PUT', data)
    
    def post_users_wallets_cards(self, card_type):
        return self.conn.consume('users/wallets/cards/' + card_type, 'POST')
    
    def delete_users_wallets_cards(self, card_id):
        return self.conn.consume('users/wallets/cards/' + card_id, 'DELETE')

demo = Demo()

# create user
response = demo.post_users()
response.raise_for_status()
user_id = response.json()['id']

# login with new user credentials
demo.authenticate()

# update user details
data = {
    'title': 'Mrs',
    'gender': 'female',
}
response = demo.put_users(data)
response.raise_for_status()

# get card types
response = demo.get_card_types()
response.raise_for_status()
card_type = response.json()['types'][0]['code']

# create card
response = demo.post_users_wallets_cards(card_type)
response.raise_for_status()
card_id = response.json()['id']

# suspend card
response = demo.delete_users_wallets_cards(card_id)
response.raise_for_status()
card_status = response.json()['status']
