# -*- coding: utf-8 -*-

from .context import Utilities

import unittest

class TestHelpersUtilitiesFunctions(unittest.TestCase):
    
    def setUp(self):
        self.utilities = Utilities()
    
    def test_is_array(self):
        is_array = self.utilities.is_array([0])
        
        self.assertTrue(is_array)
    
    def test_ksort(self):
        items = {'b': 'Nonce', 'a': 'Test'}
        expected_output = [('a', 'Test'), ('b', 'Nonce')]
        
        ksort = self.utilities.ksort(items)
        
        self.assertIsInstance(ksort, list)
        self.assertListEqual(ksort, expected_output)
    
    def test_mt_rand(self):
        mt_rand = self.utilities.mt_rand()
        
        self.assertIsInstance(mt_rand, int)
    
    def test_mb_detect_encoding(self):
        mb_detect_encoding = self.utilities.mb_detect_encoding('Test')
        
        self.assertIsInstance(mb_detect_encoding, str)
        self.assertEqual(mb_detect_encoding, 'utf-8')

if __name__ == '__main__':
    unittest.main()
