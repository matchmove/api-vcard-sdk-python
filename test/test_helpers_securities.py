# -*- coding: utf-8 -*-

from .context import Securities

import unittest, os

class TestHelpersSecuritiesFunctions(unittest.TestCase):
    
    def setUp(self):
        self.plaintext = 'first_name=Fname&last_name=Lname&mobile=470256213021&preferred_name=FnameLname&mobile_country_code=65&password=mypassword1111&email=myuser470256213021@matchmove.com'
        self.securities = Securities(os.environ['VCARD_KEY'])
        self.encrypted_data = self.securities.encrypt(self.plaintext)
    
    def test_decrypt(self):
        decrypted_data = self.securities.decrypt(self.encrypted_data)
        
        self.assertEqual(self.plaintext, decrypted_data)

if __name__ == '__main__':
    unittest.main()
