# -*- coding: utf-8 -*-

from .context import Connection
from .context import Utilities

import exceptions, httplib, os, unittest

class TestConnectionCoreFunctions(unittest.TestCase):
    
    def setUp(self):
        self.credentials = {
            'host': os.environ['VCARD_HOST'] + '/sgmc/v1',
            'key': os.environ['VCARD_KEY'],
            'secret': os.environ['VCARD_SECRET'],
            'card_type': os.environ['VCARD_CARD_TYPE'],
            'username': 'myuser470256213021@matchmove.com',
            'password': 'mypassword1111'
        }
        
        self.utilities = Utilities()
        
        self.pub_conn = Connection(
            self.credentials['host'],
            self.credentials['key'],
            self.credentials['secret']
        )
        
        self.conn = Connection(
            self.credentials['host'],
            self.credentials['key'],
            self.credentials['secret']
        )
    
    def test_connection(self):
        self.assertIsInstance(self.pub_conn, Connection)
    
    def test_get_default_query(self):
        response = self.pub_conn.get_default_query()
        expected_keys = [
            'oauth_version',
            'oauth_consumer_key',
            'oauth_signature_method',
            'oauth_nonce',
            'oauth_timestamp'
        ]
        
        self.assertIsInstance(response, dict)
        for index in range(len(expected_keys)):
            self.assertTrue(expected_keys[index] in response)
    
    def test_get_timestamp(self):
        response = self.pub_conn.get_timestamp()
        
        self.assertIsInstance(response, str)
        self.assertEqual(10, len(response))
    
    def test_get_signature(self):
        response = self.pub_conn.get_signature(
            self.credentials['secret'],
            'POST',
            'users'
        )
        
        self.assertIsInstance(response, str)
        self.assertEqual(28, len(response))
    
    def test_public_consume_invalid_method(self):
        response = self.pub_conn.consume('users/wallets/cards/types', 'PATCH')
        
        self.assertEqual(response, exceptions.AttributeError)
    
    def test_public_consume(self):
        response = self.pub_conn.consume('users/wallets/cards/types', 'GET')
        json_data = response.json()
        
        self.assertEqual(httplib.OK, response.status_code)
        
        fields = ['types', 'count']
        for index in range(len(fields)):
            self.assertTrue(fields[index] in json_data)
    
    def test_authenticated_consume(self):
        data = {
            'title': 'Mr',
            'gender': 'male'
        }
        
        self.conn.authenticate(
            self.credentials['username'],
            self.credentials['password']
        )
        response = self.conn.consume('users', 'PUT', data)
        json_data = response.json()
        
        self.assertEqual(httplib.OK, response.status_code)
        
        fields = ['id', 'email', 'name']
        for index in range(len(fields)):
            self.assertTrue(fields[index] in json_data)

if __name__ == '__main__':
    unittest.main()
