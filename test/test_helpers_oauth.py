# -*- coding: utf-8 -*-

from .context import OAuth

import unittest, os

class TestHelpersUtilitiesFunctions(unittest.TestCase):
    
    def setUp(self):
        self.credentials = {
            'host': os.environ['VCARD_HOST'] + '/sgmc/v1',
            'key': os.environ['VCARD_KEY'],
            'secret': os.environ['VCARD_SECRET']
        }
        
        self.oauth = OAuth()
    
    def test_get_nonce(self):
        get_nonce = self.oauth.get_nonce()
        
        self.assertIsInstance(get_nonce, str)
    
    def test_urlencode(self):
        text = 'test+1234@test.com'
        expected_output = 'test%201234@test.com'
        urlencode = self.oauth.urlencode(text)
        
        self.assertIsInstance(urlencode, str)
        self.assertEqual(urlencode, expected_output)
    
    def test_urlencode_list(self):
        text = ['test+1234@test.com', 'Me_Test']
        expected_output = 'test+1234@test.com&Me_Test'
        urlencode = self.oauth.urlencode(text)
        
        self.assertIsInstance(urlencode, str)
        self.assertEqual(urlencode, expected_output)
    
    def test_encrypt(self):
        signature = '0983903b0c1f4a297c47a31625c06e68ab2fdedf'
        payload = 'test+1234@test.com&Me_Test'
        encrypt = self.oauth.encrypt(
            self.credentials['secret'],
            signature,
            payload
        )
        
        self.assertIsInstance(encrypt, str)
        self.assertEqual(len(encrypt), 64)

if __name__ == '__main__':
    unittest.main()
